# 'multi_robot_task_execution' Package

The `multi_robot_task_execution` package provides connection system between multi PCs for multi robots task executions.

*   Maintainer: Ishikawa Tomochika ([ishikawa.tomochika@em.ci.ritsumei.ac.jp](mailto:ishikawa.tomochika@em.ci.ritsumei.ac.jp))
*   Author: Ishikawa Tomochika ([ishikawa.tomochika@em.ci.ritsumei.ac.jp](mailto:ishikawa.tomochika@em.ci.ritsumei.ac.jp)).

## /src

### `task_decomposition_client.py`
GPT-4によるタスク分解を行い、分解されたそれぞれのタスクをマルチロボットを動かすそれぞれのPCへ送信するプログラム。
13~16行目でタスクの送信先となるPCのIPアドレスを指定。

#### パラメータ
*   **first_server_ip** (string, default:10.40.20.106): 1つ目の通信するPCのIPアドレス
*   **second_server_ip** (string, default:10.40.20.131): 2つ目の通信するPCのIPアドレス
*   **server_port** (int, default: 12345): 通信するポート番号(未使用なら何でもいい)

### `task_decomposition_client_csv.py`
`task_decomposition_client.py`を実行すると、タスク分解結果が`/data`フォルダにcsv形式で保存される。
csvに保存されたタスク分解結果を読み出してそれをマルチロボットを動かすそれぞれのPCへ送信するプログラム。

### `wait_task.py`
タスク分解された結果をタスクをマルチロボットを動かすそれぞれのPCが受け取るプログラム。

#### パラメータ
*   **server_ip** (string, default:10.40.20.106): このプログラムを実行するPCのIPアドレス(自動設定)
*   **server_port** (int, default: 12345): 通信するポート番号。`task_decomposition_client.py`で指定する番号と同一のものを使用

## 実行手順

*   `wait_task.py`をマルチロボットを動かすそれぞれのPCで実行。
*   `task_decomposition_client.py`(`task_decomposition_client_csv.py`)をGPT-4によるタスク分解を行うPCで実行。
