import rospy
import openai
import sys
import re
import roslib
import csv
import subprocess
from datetime import datetime
import socket
import threading
import select

###########GPT-4によるタスク分解#############
rospy.loginfo("Start decomposing task with GPT-4")
API_KEY_FILE = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/include/API_key.txt"
# APIキーをファイルから読み込む
try:
    with open(API_KEY_FILE, "r") as file:
        openai.api_key = file.read().strip()  # .strip() で余分な空白や改行を削除
except Exception as e:
    sys.exit(f"APIキーの読み込みに失敗しました: {e}")

# Task_Decomposition.txtの内容を読み込む
try:
    Task_Decomposition_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/include/Task_Decomposition.txt"
    with open(Task_Decomposition_path, "r", encoding="utf-8") as f:
        task_template = f.read()
except Exception as e:
    sys.exit(f"タスクファイルの読み込みに失敗しました: {e}")

# ユーザーの入力を受け取る
task_description = input("Task Description: ")

# ユーザーの入力をファイルの内容に結合
full_task = task_template.replace("# Task Description: ", "# Task Description: " + task_description)

# print(full_task)

# OpenAI APIに送信するメッセージを作成
messages = [{'role': 'user', 'content': full_task}]

# OpenAI APIを呼び出してレスポンスを取得
try:
    response = openai.ChatCompletion.create(
        model='gpt-4',
        messages=messages,
        temperature=0.0,
    )
    # print('\n' + response['choices'][0]['message']['content'] + '\n')
except openai.error.OpenAIError as e:
    sys.exit(f"OpenAI APIエラー: {e}")
except Exception as e:
    sys.exit(f"エラーが発生しました: {e}")

task_description_output = ('\n' + response['choices'][0]['message']['content'] + '\n')

# print(task_description_output)


# Coalition_Formation.txtの内容を読み込む
try:
    Coalition_Formation_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/include/Coalition_Formation.txt"
    with open(Coalition_Formation_path, "r", encoding="utf-8") as f:
        Coalition_Formation = f.read()
except Exception as e:
    sys.exit(f"タスクファイルの読み込みに失敗しました: {e}")


# task_descriptionの出力をファイルの内容に結合
full_Coalition = Coalition_Formation.replace("# GENERAL TASK DECOMPOSITION", task_description_output)

# print(full_Coalition)

# OpenAI APIに送信するメッセージを作成
messages = [{'role': 'user', 'content': full_Coalition}]

# OpenAI APIを呼び出してレスポンスを取得
try:
    response = openai.ChatCompletion.create(
        model='gpt-4',
        messages=messages,
        temperature=0.0,
    )
    # print('\n' + response['choices'][0]['message']['content'] + '\n')
except openai.error.OpenAIError as e:
    sys.exit(f"OpenAI APIエラー: {e}")
except Exception as e:
    sys.exit(f"エラーが発生しました: {e}")

Coalition_Formation_output = ('\n' + response['choices'][0]['message']['content'] + '\n')

# 抽出したい単語
word1 = 'Robot1: '
word2 = 'Robot2: '

# 正規表現パターン（単語の後に続く任意の文字列をマッチさせる）
pattern1 = word1 + r'([^.]*)'
pattern2 = word2 + r'([^.]*)'

# パターンに一致するすべての部分を抽出
matches1 = re.findall(pattern1, Coalition_Formation_output, re.IGNORECASE)
matches2 = re.findall(pattern2, Coalition_Formation_output, re.IGNORECASE)
decomposed_tasks = [matches1[0], matches2[0]]

task_data = [
    ['whole_task', 'robot1_task', 'robot2_task'],
    [task_description, matches1[0], matches2[0]]
]

# 現在の年月日時分を取得
current_datetime = datetime.now()
# テキストで出力（指定された形式）
formatted_datetime = current_datetime.strftime("%Y%m%d%H%M")

# CSVファイルへの書き込み
# 出力ファイルのパス
csv_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + '/data/' + formatted_datetime + ".csv"

with open(csv_path, 'w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file)
    
    # データをCSVファイルに書き込む
    csv_writer.writerows(task_data)

###########他の2PCに分解されたタスクを送信#############
rospy.loginfo("Standby to send decomposed tasks")
def kill_process_by_port(port_number):
    try:
        # lsofコマンドを使用してポート番号に対応するプロセスのPIDを取得
        result = subprocess.run(['lsof', '-i', f':{port_number}'], capture_output=True, text=True)
        lines = result.stdout.split('\n')
        process_info = [line.split()[1] for line in lines[1:] if line]

        if process_info:
            # 取得したPIDを使ってプロセスを終了
            subprocess.run(['kill', '-9', process_info[0]])
            print(f"Process on port {port_number} terminated.")
        else:
            print(f"No process found on port {port_number}.")

    except Exception as e:
        print(f"An error occurred: {e}")

kill_process_by_port(12345)
kill_process_by_port(12346)

def handle_client(client_socket, client_number):
    try:
        # クライアントからのメッセージを受信
        received_message = client_socket.recv(1024).decode('utf-8')
        print(f"Message from client {client_number}: {received_message}")

        # クライアントにメッセージを送信
        client_socket.sendall(decomposed_tasks[int(client_number-1)].encode('utf-8'))

    except BrokenPipeError:
        # クライアントが接続を切断した場合の処理
        print(f"Client {client_number} disconnected")

    finally:
        # ソケットを閉じる
        client_socket.close()

# サーバのIPアドレスとポート番号
server_port_1 = 12345
server_port_2 = 12346
server_ip = '10.40.20.106'

# ソケットを作成
server_socket_1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket_2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# サーバを指定したIPアドレスとポートでバインド
server_socket_1.bind((server_ip, server_port_1))
server_socket_2.bind((server_ip, server_port_2))

# 接続を待機
server_socket_1.listen()
server_socket_2.listen()

print(f"Server is waiting at {server_ip}:{server_port_1} and {server_port_2} ...")

try:
    while True:
        # クライアントからの接続を待機
        readable, _, _ = select.select([server_socket_1, server_socket_2], [], [])

        for sock in readable:
            if sock is server_socket_1:
                client_socket_1, client_address_1 = server_socket_1.accept()
                print(f"Connected to first client: {client_address_1} (port {server_port_1})")
                client_thread_1 = threading.Thread(target=handle_client, args=(client_socket_1, 1))
                client_thread_1.start()

            elif sock is server_socket_2:
                client_socket_2, client_address_2 = server_socket_2.accept()
                print(f"Connected to second client: {client_address_2} (port {server_port_2})")
                client_thread_2 = threading.Thread(target=handle_client, args=(client_socket_2, 2))
                client_thread_2.start()

except KeyboardInterrupt:
    # Ctrl+Cでプログラムを終了する場合
    print("Server terminated by user")

finally:
    # ソケットを閉じる
    server_socket_1.close()
    server_socket_2.close()