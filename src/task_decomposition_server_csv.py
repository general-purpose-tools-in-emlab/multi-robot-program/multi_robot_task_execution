import rospy
import openai
import sys
import re
import roslib
import csv
import subprocess
from datetime import datetime
import socket
import threading
import select

###########csvファイルに保存されているGPT-4によるタスク分解を利用#############
csv_file_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/data/test.csv"

# ファイルを開いてCSVリーダーを作成
with open(csv_file_path, 'r', newline='', encoding='utf-8') as csvfile:
    csv_reader = csv.reader(csvfile)
    # ヘッダーを取得
    headers = next(csv_reader)
    # データを読み込む
    task_data = [row for row in csv_reader]

# 結果の表示
decomposed_tasks = [task_data[0][1], task_data[0][2]]

###########他の2PCに分解されたタスクを送信#############
rospy.loginfo("Standby to send decomposed tasks")
def kill_process_by_port(port_number):
    try:
        # lsofコマンドを使用してポート番号に対応するプロセスのPIDを取得
        result = subprocess.run(['lsof', '-i', f':{port_number}'], capture_output=True, text=True)
        lines = result.stdout.split('\n')
        process_info = [line.split()[1] for line in lines[1:] if line]

        if process_info:
            # 取得したPIDを使ってプロセスを終了
            subprocess.run(['kill', '-9', process_info[0]])
            print(f"Process on port {port_number} terminated.")
        else:
            print(f"No process found on port {port_number}.")

    except Exception as e:
        print(f"An error occurred: {e}")

kill_process_by_port(12345)
kill_process_by_port(12346)

def handle_client(client_socket, client_number):
    try:
        # クライアントからのメッセージを受信
        received_message = client_socket.recv(1024).decode('utf-8')
        print(f"Message from client {client_number}: {received_message}")

        # クライアントにメッセージを送信
        client_socket.sendall(decomposed_tasks[int(client_number-1)].encode('utf-8'))

    except BrokenPipeError:
        # クライアントが接続を切断した場合の処理
        print(f"Client {client_number} disconnected")

    finally:
        # ソケットを閉じる
        client_socket.close()

# サーバのIPアドレスとポート番号
server_port_1 = 12345
server_port_2 = 12346
server_ip = '10.40.20.106'

# ソケットを作成
server_socket_1 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
server_socket_2 = socket.socket(socket.AF_INET, socket.SOCK_STREAM)

# サーバを指定したIPアドレスとポートでバインド
server_socket_1.bind((server_ip, server_port_1))
server_socket_2.bind((server_ip, server_port_2))

# 接続を待機
server_socket_1.listen()
server_socket_2.listen()

print(f"Server is waiting at {server_ip}:{server_port_1} and {server_port_2} ...")

try:
    while True:
        # クライアントからの接続を待機
        readable, _, _ = select.select([server_socket_1, server_socket_2], [], [])

        for sock in readable:
            if sock is server_socket_1:
                client_socket_1, client_address_1 = server_socket_1.accept()
                print(f"Connected to first client: {client_address_1} (port {server_port_1})")
                client_thread_1 = threading.Thread(target=handle_client, args=(client_socket_1, 1))
                client_thread_1.start()

            elif sock is server_socket_2:
                client_socket_2, client_address_2 = server_socket_2.accept()
                print(f"Connected to second client: {client_address_2} (port {server_port_2})")
                client_thread_2 = threading.Thread(target=handle_client, args=(client_socket_2, 2))
                client_thread_2.start()

except KeyboardInterrupt:
    # Ctrl+Cでプログラムを終了する場合
    print("Server terminated by user")

finally:
    # ソケットを閉じる
    server_socket_1.close()
    server_socket_2.close()