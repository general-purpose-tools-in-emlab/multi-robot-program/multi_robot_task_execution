import rospy
import sys
import re
import roslib
import csv
import subprocess
from datetime import datetime
import socket
import threading
import select

###########サーバのIPアドレスとポート番号##############
first_server_ip = '10.40.20.106' #通信するPCのip
second_server_ip = '10.40.20.131' #通信するPCのip
server_port = 12345 #なんでもいい

###########csvファイルに保存されているGPT-4によるタスク分解を利用#############
csv_file_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/data/test.csv"

# ファイルを開いてCSVリーダーを作成
with open(csv_file_path, 'r', newline='', encoding='utf-8') as csvfile:
    csv_reader = csv.reader(csvfile)
    # ヘッダーを取得
    headers = next(csv_reader)
    # データを読み込む
    task_data = [row for row in csv_reader]

# 結果の表示
decomposed_tasks = [task_data[0][1], task_data[0][2]]

###########他の2PCに分解されたタスクを送信#############
rospy.loginfo("Standby to send decomposed tasks")
def send_task(server_ip, server_port, client_number):
    try:
        # サーバーに接続
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((server_ip, server_port))

        # タスクの送信
        client_socket.sendall(decomposed_tasks[int(client_number-1)].encode('utf-8'))

        # タスクの受信
        received_task = client_socket.recv(1024).decode('utf-8')
        print(f"Response from client: {received_task}")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        # ソケットを閉じる
        client_socket.close()

# クライアント1がタスクを要求
send_task(first_server_ip, server_port, 1)

# クライアント2がタスクを要求
send_task(second_server_ip, server_port, 2)