import rospy
import openai
import sys
import re
import roslib
import csv
import subprocess
from datetime import datetime
import socket
import threading
import select

###########サーバのIPアドレスとポート番号##############
first_server_ip = '10.40.20.106' #通信するPCのip
second_server_ip = '10.40.20.131' #通信するPCのip
server_port = 12345 #なんでもいい

###########GPT-4によるタスク分解#############
rospy.loginfo("Start decomposing task with GPT-4")
API_KEY_FILE = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/include/API_key.txt"
# APIキーをファイルから読み込む
try:
    with open(API_KEY_FILE, "r") as file:
        openai.api_key = file.read().strip()  # .strip() で余分な空白や改行を削除
except Exception as e:
    sys.exit(f"APIキーの読み込みに失敗しました: {e}")

# Task_Decomposition.txtの内容を読み込む
try:
    Task_Decomposition_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/include/Task_Decomposition.txt"
    with open(Task_Decomposition_path, "r", encoding="utf-8") as f:
        task_template = f.read()
except Exception as e:
    sys.exit(f"タスクファイルの読み込みに失敗しました: {e}")

# ユーザーの入力を受け取る
task_description = input("Task Description: ")

# ユーザーの入力をファイルの内容に結合
full_task = task_template.replace("# Task Description: ", "# Task Description: " + task_description)

# print(full_task)

# OpenAI APIに送信するメッセージを作成
messages = [{'role': 'user', 'content': full_task}]

# OpenAI APIを呼び出してレスポンスを取得
try:
    response = openai.ChatCompletion.create(
        model='gpt-4',
        messages=messages,
        temperature=0.0,
    )
    # print('\n' + response['choices'][0]['message']['content'] + '\n')
except openai.error.OpenAIError as e:
    sys.exit(f"OpenAI APIエラー: {e}")
except Exception as e:
    sys.exit(f"エラーが発生しました: {e}")

task_description_output = ('\n' + response['choices'][0]['message']['content'] + '\n')

# print(task_description_output)


# Coalition_Formation.txtの内容を読み込む
try:
    Coalition_Formation_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + "/include/Coalition_Formation.txt"
    with open(Coalition_Formation_path, "r", encoding="utf-8") as f:
        Coalition_Formation = f.read()
except Exception as e:
    sys.exit(f"タスクファイルの読み込みに失敗しました: {e}")


# task_descriptionの出力をファイルの内容に結合
full_Coalition = Coalition_Formation.replace("# GENERAL TASK DECOMPOSITION", task_description_output)

# print(full_Coalition)

# OpenAI APIに送信するメッセージを作成
messages = [{'role': 'user', 'content': full_Coalition}]

# OpenAI APIを呼び出してレスポンスを取得
try:
    response = openai.ChatCompletion.create(
        model='gpt-4',
        messages=messages,
        temperature=0.0,
    )
    # print('\n' + response['choices'][0]['message']['content'] + '\n')
except openai.error.OpenAIError as e:
    sys.exit(f"OpenAI APIエラー: {e}")
except Exception as e:
    sys.exit(f"エラーが発生しました: {e}")

Coalition_Formation_output = ('\n' + response['choices'][0]['message']['content'] + '\n')

# 抽出したい単語
word1 = 'Robot1: '
word2 = 'Robot2: '

# 正規表現パターン（単語の後に続く任意の文字列をマッチさせる）
pattern1 = word1 + r'([^.]*)'
pattern2 = word2 + r'([^.]*)'

# パターンに一致するすべての部分を抽出
matches1 = re.findall(pattern1, Coalition_Formation_output, re.IGNORECASE)
matches2 = re.findall(pattern2, Coalition_Formation_output, re.IGNORECASE)
decomposed_tasks = [matches1[0], matches2[0]]

task_data = [
    ['whole_task', 'robot1_task', 'robot2_task'],
    [task_description, matches1[0], matches2[0]]
]

# 現在の年月日時分を取得
current_datetime = datetime.now()
# テキストで出力（指定された形式）
formatted_datetime = current_datetime.strftime("%Y%m%d%H%M")

# CSVファイルへの書き込み
# 出力ファイルのパス
csv_path = roslib.packages.get_pkg_dir('multi_robot_task_execution') + '/data/' + formatted_datetime + ".csv"

with open(csv_path, 'w', newline='') as csv_file:
    csv_writer = csv.writer(csv_file)
    
    # データをCSVファイルに書き込む
    csv_writer.writerows(task_data)

###########他の2PCに分解されたタスクを送信#############
rospy.loginfo("Standby to send decomposed tasks")
def send_task(server_ip, server_port, client_number):
    try:
        # サーバーに接続
        client_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
        client_socket.connect((server_ip, server_port))

        # タスクの送信
        client_socket.sendall(decomposed_tasks[int(client_number-1)].encode('utf-8'))

        # タスクの受信
        received_task = client_socket.recv(1024).decode('utf-8')
        print(f"Task received from server: {received_task}")

    except Exception as e:
        print(f"An error occurred: {e}")

    finally:
        # ソケットを閉じる
        client_socket.close()

# クライアント1がタスクを要求
send_task(first_server_ip, server_port, 1)

# クライアント2がタスクを要求
send_task(second_server_ip, server_port, 2)