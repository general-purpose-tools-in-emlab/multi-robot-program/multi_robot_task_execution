import socket
import subprocess

# サーバのIPアドレスとポート番号
# コマンドを実行して結果を取得
result = subprocess.run(['hostname', '-I'], capture_output=True, text=True)
# 標準出力の内容を取得
ip_addresses = result.stdout.strip()
server_ip = ip_addresses.split()[0]
server_port = 12345
# ソケットを作成
server_socket = socket.socket(socket.AF_INET, socket.SOCK_STREAM)
# サーバを指定したIPアドレスとポートでバインド
server_socket.bind((server_ip, server_port))
# 接続を待機
server_socket.listen()
print(f"Server is waiting at {server_ip}:{server_port} ...")

try:
    while True:
        # クライアントからの接続を待機
        client_socket, client_address = server_socket.accept()
        print(f"Connected to first client: {client_address}")

        # クライアントからのメッセージを受信
        received_message = client_socket.recv(1024).decode('utf-8')
        print(f"Message from first client: {received_message}")

        # クライアントにメッセージを送信
        client_socket.sendall("Hello, first Client!".encode('utf-8'))

except BrokenPipeError:
    # クライアントが接続を切断した場合の処理
    print("Client disconnected")

finally:
    # ソケットを閉じる
    if 'client_socket' in locals():
        client_socket.close()

    server_socket.close()
